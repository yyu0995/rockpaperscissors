## Download the app:

1. Click the **...** button under the **Source** heading.
2. Click the **Download repository** button.
3. Select your local directory where you want to download this app.
4. The zip file will be downloaded into this directory.

## Build the app (Environment setup):

1. Install the Java Development Kit from [here](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html).
2. Download the Android SDK. You may install the SDK using [Android Studio](https://developer.android.com/).
3. Enable USB debugging on your device.
4. Configure the Android SDK path in Unity. You may use the Android Studio SDK Manager to do that, 
   click Tools > Android > SDK Manager or click SDK Manager in the toolbar.
5. Download and set up the Android NDK from [here](https://developer.android.com/ndk/downloads/index.html).
6. Now, you can open the app and build it using Android Studio.

## Install and run the app on your mobile device:

1. Supposing you are using Windows, you need to install the Google USB Driver: 
   1-1. In Android Studio, click Tools > SDK Manager;
   1-2. Click the SDK Tools tab;
   1-3. Select Google USB Driver and click OK.
2. Proceed to install the package. When done, the driver files are downloaded into
   the android_sdk\extras\google\usb_driver\ directory.
3. Then,  you can click **Run** in Android Studio to build and run your app on the device.

## License:

This app is using the BSD license. Becasue this app should place minimal restrictions on future behavior. 
Developers should be couraged to spend their time creating and promoting excellent code 
instead of worrying license violating.