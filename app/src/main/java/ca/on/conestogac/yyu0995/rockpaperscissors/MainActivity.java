/******************
 * Create date: September 21, 2020
 * Writer: Yilong Yu
 * Description: Conestoga College PROG3210 Assignment1 *
 */
package ca.on.conestogac.yyu0995.rockpaperscissors;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class MainActivity extends AppCompatActivity {

    private Button btnRock;
    private Button btnPaper;
    private Button btnScissors;

    private ImageView imgComputer;
    private ImageView imgPlayer;

    private TextView txtResult;

    // Declare constants for generate random integers 1, 2, or 3
    private final int MIN_INTEGER = 1;
    private final int MAX_INTEGER = 3;

    // Declare global flags variables to remember screen status
    // before switching orientations or deactivating and activating
    // 0: rock; 1: paper; 2: scissors
    private int playerActionFlag;
    private int computerActionFlag;

    private SharedPreferences sharedPref;
    private final String PREF_COMPUTER_KEY = "lastComputerAction";
    private final String PREF_PLAYER_KEY = "lastPlayerAction";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            // Declare a variable of general listener
            View.OnClickListener rockPaperScissorsListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (view.getId()) {
                        case R.id.btnRock:
                            displayAction(imgPlayer, R.drawable.ic_rock);
                            playerActionFlag = 0;
                            break;
                        case R.id.btnPaper:
                            displayAction(imgPlayer, R.drawable.ic_paper);
                            playerActionFlag = 1;
                            break;
                        case R.id.btnScissors:
                            displayAction(imgPlayer, R.drawable.ic_scissors);
                            playerActionFlag = 2;
                            break;
                    }

                    computerActionFlag = generateComputerAction();

                    displayResult(computerActionFlag, playerActionFlag);
                }
            };

            //R: resources
            setTheme(R.style.AppTheme);     // turn off splash theme and turn back on original theme

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            btnRock = findViewById(R.id.btnRock);
            btnPaper = findViewById(R.id.btnPaper);
            btnScissors = findViewById(R.id.btnScissors);

            imgComputer = findViewById(R.id.imageViewComputer);
            imgPlayer = findViewById(R.id.imageViewPlayer);

            txtResult = findViewById(R.id.textViewResult);

            // Use an anonymous class as the listener
            btnRock.setOnClickListener(rockPaperScissorsListener);
            btnPaper.setOnClickListener(rockPaperScissorsListener);
            btnScissors.setOnClickListener(rockPaperScissorsListener);

            sharedPref = getSharedPreferences("lastActions", MODE_PRIVATE);

            // Reset flags to display initial interface
            playerActionFlag = -1;
            computerActionFlag = -1;
        }
        catch (Exception ex)
        {
            Toast.makeText(this, R.string.error + ":" + ex.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }

    // Assign picture onto ImageView
    private void displayAction(ImageView img, int id)
    {
        img.setImageResource(id);
        img.setVisibility(View.VISIBLE);
    }

    // Generate random integer for determine computer's action
    private int getRandomInt(int min, int max)
    {
        return ( max >= min
                 ? min + (int)Math.floor(Math.random() * (max - min + 1))
                 : getRandomInt(max, min)
               );
    }

    private int generateComputerAction()
    {
        int randomInt = getRandomInt(MIN_INTEGER, MAX_INTEGER); // random integer among 1, 2, 3

        switch (randomInt)
        {
            case 1:
                displayAction(imgComputer, R.drawable.ic_rock);
                return  0;
            case 2:
                displayAction(imgComputer, R.drawable.ic_paper);
                return  1;
            case 3:
                displayAction(imgComputer, R.drawable.ic_scissors);
                return 2;
        }

        return -1;
    }

    private void displayResult(int computerActionFlag, int playerActionFlag)
    {
        if (!(computerActionFlag == -1 && playerActionFlag == -1)){
            switch (computerActionFlag - playerActionFlag)
            {
                case 1:
                case -2:
                    txtResult.setText(R.string.computer_win);
                    break;
                case 0:
                    txtResult.setText(R.string.is_tie);
                    break;
                default:
                    txtResult.setText(R.string.player_win);
            }
        }
        else
        {
            txtResult.setText(R.string.instroduction);
        }
    }

    @Override
    protected void onPause() {
        Editor editor = sharedPref.edit();

        editor.putInt(PREF_COMPUTER_KEY, computerActionFlag);
        editor.putInt(PREF_PLAYER_KEY, playerActionFlag);
        editor.commit();

        //Toast.makeText(this, "onPause called", Toast.LENGTH_SHORT).show();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        computerActionFlag = sharedPref.getInt(PREF_COMPUTER_KEY, -1);
        playerActionFlag = sharedPref.getInt(PREF_PLAYER_KEY, -1);

        reassignImage(imgComputer, computerActionFlag);
        reassignImage(imgPlayer, playerActionFlag);

        if (!(computerActionFlag == -1 && playerActionFlag == -1)){
            imgComputer.setVisibility(View.VISIBLE);
            imgPlayer.setVisibility(View.VISIBLE);
        }

        displayResult(computerActionFlag, playerActionFlag);
        //Toast.makeText(this, "onResume called", Toast.LENGTH_SHORT).show();
    }

    // Re-assign pictures after switching orientations or deactivating and activating
    private void reassignImage(ImageView img, int flag)
    {
        switch (flag)
        {
            case 0:
                img.setImageResource(R.drawable.ic_rock);
                break;
            case 1:
                img.setImageResource(R.drawable.ic_paper);
                break;
            case 2:
                img.setImageResource(R.drawable.ic_scissors);
                break;
        }
    }
}